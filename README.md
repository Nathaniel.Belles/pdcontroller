# pdController

A USB-C Power Delivery controller board for software and hardware development and testing. Terminates the power lines to screw terminals and has switches and a rotary encoder for changing settings and an OLED display for viewing voltage and current levels. 

<img src="hardware/exports/model_front.png" height="250px"><img src="hardware/exports/model_back.png" height="250px">


## Table of Contents
- [pdController](#pdcontroller)
  - [Table of Contents](#table-of-contents)
  - [Background](#background)
  - [About The Project](#about-the-project)
  - [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Ordering and Manufacturing](#ordering-and-manufacturing)
    - [Installation](#installation)
      - [Firmware](#firmware)
  - [Usage](#usage)
  - [Design Notes](#design-notes)
  - [Contributing](#contributing)


## Background

A couple months ago, my friend approached me about creating a neon-style LED sign for one of our favorite late-night snack places, Cook Out. After creating the sign, we decided it would be cool to independently control each of the light segments from a USB-C Power Delivery power supply. Before creating that PCB I decided it would good to create a test PCB for verifying the USB-C Power Delivery portion of the circuit. Enter: this project. This circuit will allow me to test the hardware and software aspects (because a lot of IC's are lacking in documentation and I'm planning on pulling code from a lot of other public projects). Once this PCB and firmware are verified to be working I'll integrate this circuit into the full circuit in the other project. If you want to see how the full circuit turns out, check out it's project [here](https://gitlab.com/Nathaniel.Belles/iotusbcpdc).

## About The Project

This project is based on the FUSB302B USB Type-C Controller with Power Delivery IC. The FUSB302B is used for all communication and negotiation of Power Delivery and can be configured over the I2C bus. This project combines the FUSB302B with USB Type-C connector, screw terminals, microcontroller, and interface buttons/LEDs/screen. 

For the hardware side of the project, I created a PCB based on circuit diagrams shown in the [FUSB302B datasheet](https://www.onsemi.com/pdf/datasheet/fusb302b-d.pdf) with additional circuits from other similar projects online. I added in some MOSFETs and related supporting circuitry for switching the output voltage on and off on three distinct sets of screw terminals.

For the software, I found a [project](https://github.com/ryan-ma/PD_Micro) that handles the communication between a microcontroller and the FUSB302B IC with selectable default behaviors. It seems to implement the Power Delivery 3.0 PPS interface as well, making it possible to select the output voltage on the rail. 

## Getting Started
There are a couple of different parts to this project. The first is the hardware that runs the USB-C communication interface which requires the PCB that has been designed. Once the PCB has been manufactured, the `pdcontroller.ino` Arduino sketch needs to be uploaded to the device. Then you are ready to start interfacing with USB-C Power Delivery. 

- Looking for how to order your own? Check out the section on [Ordering and Manufacturing](#ordering-and-manufacturing).
- Looking for how to upload the code to the device? Check out the section on [Firmware Installation](#firmware).


### Prerequisites
I am assuming you have some very basic knowledge of microcontrollers, communication protocols, electrical wiring, and computer usage.

### Ordering and Manufacturing
For information on how to order the PCB, check out the [ordering_info](/hardware/exports/ordering_info.png) screenshot of the parameters I selected while ordering these PCB's from JLCPCB. Once the PCB's have arrived, follow the schematic to solder all the components to the PCB.

### Installation

#### Firmware
To program the device, you can upload the `pdcontroller.ino` file to the ATtiny1616 using the [Arduino IDE](https://www.arduino.cc/en/software) and [MegaTinyCore](https://github.com/SpenceKonde/megaTinyCore) by connecting your programmer to the UPDI header on the PCB. If you need help programming the ATtiny, check out my other project, [updiProgrammer](https://gitlab.com/Nathaniel.Belles/updiprogrammer) which goes into detail about how to use a simple USB to UART adapter and a couple passive components to upload Arduino sketches to the ATtiny. There are of course many ways to upload an Arduino sketch to an ATtiny, feel free to use whatever platform you are most comfortable using, the sketch is not specific to any particular programmer. 

## Usage
The usage of this device will be decided once a lot more of the code has been written in the `pdcontroller.ino` file. I will update this usage guide as I get closer to finishing the code. 

## Design Notes
Here are a couple of notes on why I made the choices I made while designing this device. This section will most likely be placeholders while I'm testing certain features where I won't make the most optimal design decision (size, part-count, or efficiency) and instead make something easy to debug or test certain features. 

- As this device is focused around development, it is not nearly as compact as it could be. It also has a lot more debugging pins available and potential places for optimization. I also have included some potentially unnecessary components that I did not fully populate during the assembly process. In the future, I will try to reduce these as much as possible. 
- If you are trying to use this device in battery applications, you may want to consider changing this design to replace the 3.3V linear regulator with a 3.3V buck converter. This will probably help with decreasing the quiescent current that this device takes away from your battery. I chose to use a linear regulator because the part count and price are hard to beat and my application will mainly be focused around being plugged into wall power. It probably doesn't amount to much but in battery applications, every little bit counts. 

## Contributing
Feel free to contribute to this project. I welcome you to open issues, make pull requests, or just fork it and add your own features!
