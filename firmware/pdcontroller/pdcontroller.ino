#include <Wire.h>
#include "PD_UFP.h"

class PD_UFP_c pd;

void setup()
{
    Wire.begin();
    pd.init(PD_POWER_OPTION_MAX_POWER);
    pd.set_output(true);
    pinMode(16, OUTPUT);
    pinMode(0, OUTPUT);
    pinMode(1, OUTPUT);
    analogWrite(16, 255);
    analogWrite(0, 0);
    analogWrite(1, 0);
    delay(5000);
}

void loop()
{
    int counter = 1;
    unsigned long start_time = millis();
    analogWrite(16, 192);
    // analogWrite(1, 192);
    pd.set_power_option(PD_POWER_OPTION_MAX_5V);
    while (millis() - start_time < counter * 5000)
    {
        if (pd.is_power_ready())
        {
            analogWrite(0, 192);
            // pd.set_output(true);
        }
        pd.run();
    }
    counter++;
    // pd.set_output(false);
    analogWrite(16, 127);
    // analogWrite(1, 127);
    pd.set_power_option(PD_POWER_OPTION_MAX_9V);
    while (millis() - start_time < counter * 5000)
    {
        if (pd.is_power_ready())
        {
            analogWrite(0, 127);
            // pd.set_output(true);
        }
        pd.run();
    }
    counter++;
    // pd.set_output(false);
    analogWrite(16, 64);
    // analogWrite(1, 64);
    pd.set_power_option(PD_POWER_OPTION_MAX_15V);
    while (millis() - start_time < counter * 5000)
    {
        if (pd.is_power_ready())
        {
            analogWrite(0, 64);
            // pd.set_output(true);
        }
        pd.run();
    }
    counter++;
    // pd.set_output(false);
    analogWrite(16, 0);
    // analogWrite(1, 0);
    pd.set_power_option(PD_POWER_OPTION_MAX_20V);
    while (millis() - start_time < counter * 5000)
    {
        if (pd.is_power_ready())
        {
            analogWrite(0, 0);
            // pd.set_output(true);
        }
        pd.run();
    }
    counter++;
    // pd.set_output(false);
}